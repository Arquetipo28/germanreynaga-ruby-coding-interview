require 'rails_helper'

RSpec.describe Company, type: :model do
  it { should have_db_column(:name).of_type(:string) }
end
