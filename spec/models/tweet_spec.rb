require 'rails_helper'

RSpec.describe Tweet, type: :model do
  it { should have_db_column(:body).of_type(:text) }

  it { should validate_presence_of(:body) }
end
