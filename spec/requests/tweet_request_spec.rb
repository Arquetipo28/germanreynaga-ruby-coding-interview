require 'rails_helper'

RSpec.describe "Tweets", type: :request do
  describe '#index' do
    shared_context 'with_tweets' do
      before do
        user = create(:user)
        create_list(:tweet, 12, user: user)
      end
    end

    context 'when no filters are sent' do
      include_context 'with_tweets'

      subject(:current_tweets) { JSON.parse(response.body)}

      it 'returns 10 tweets' do
        get tweets_path()

        expect(current_tweets.size).to eq 10
      end
    end

    context 'when last_id=10 is sent' do
      include_context 'with_tweets'

      subject(:current_tweets) { JSON.parse(response.body) }

      it 'returns last 2 tweets' do
        get tweets_path(last_id: 10)

        expect(current_tweets.size).to eq 2
      end
    end

    context 'when username is sent' do
      include_context 'with_tweets'

      subject(:user_tweets) { JSON.parse(response.body) }

      let(:user) { create(:user, username: 'tester') }

      before do
        create(:tweet, user: user)
      end

      it 'returns only user tweets' do
        get tweets_path(username: user.username)

        expect(user_tweets.size).to eq 1
      end
    end
  end
end
