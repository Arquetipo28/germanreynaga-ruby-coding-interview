FactoryBot.define do
  factory :tweet do
    body { Faker::Twitter.status[:text] }
  end
end
