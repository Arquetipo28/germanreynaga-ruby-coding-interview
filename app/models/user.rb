class User < ApplicationRecord
  belongs_to :company
  has_many :tweets

  validates :display_name, presence: true
  validates :username, presence: true
  validates :email, presence: true

  scope :by_company, -> (identifier) { where(company: identifier) if identifier.present? }
  scope :by_username, -> (username) { where("users.username LIKE ?", "%#{username}%") if username.present? }
end
