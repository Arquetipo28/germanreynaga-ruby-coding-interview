class Tweet < ApplicationRecord
  belongs_to :user

  validates :body, presence: true

  scope :by_page, -> (last_id) { where('tweets.id > ?', last_id) }
  scope :by_user, -> (user_identifier) do
    return unless user_identifier.present?

    joins(:user).merge(User.by_username(user_identifier)) 
  end
end
