class TweetsController < ApplicationController
  DEFAULT_INITIAL = 0
  DEFAULT_LIMIT = 10

  def index
    last_record_id = filtering_params.fetch(:last_id, DEFAULT_INITIAL)
    tweets = Tweet.by_user(filtering_params[:username])
                  .by_page(last_record_id)
                  .limit(DEFAULT_LIMIT)                 

    render json: tweets
  end

  def filtering_params
    params.permit(:last_id, :username)
  end
end
